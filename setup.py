from setuptools import find_packages, setup

setup(
    name='rr_python',
    packages=find_packages(include=['rr_python']),
    version='0.1.0',
    description='Custom Python logging - Road Runner',
    license='MIT',
    install_requires=["urllib3", "requests"],
    setup_requires=['pytest-runner'],
    tests_require=['pytest==4.4.1'],
    test_suite='tests'
)