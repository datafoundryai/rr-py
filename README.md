##RR-Python

##### **Installation:**

Installation of rr_python requires the necessary credentials to the private repository and the index url to be added in the requirements.txt file and then adding `rr_python` to the list of required packages.

    `--index-url https://<username>:<password>@datafoundry.jfrog.io/artifactory/api/pypi/pypi/simple`
add this with the necessary credentials to the top of the requirements.txt file
next continue to install all requirements from the requirements file as usual:

    `pip install -r requirements.txt --no-cache-dir`
    
_Note:_ --no-cache-dir is important to avoid any issues with version problems with respect to previously installed rr_python and any changes in the repository



The steps to use RR-Python in any given python package for logging:

##### **Initialisation**

To initialise rr_python package for logging in your service, first import the "rr_python" module from rr_python as follows:
    
    `from rr_python import rr_python as rr`
  then proceed to invoke the setup function as :
    
    `rr.setupLogHandler(appName, defaultLogLevel)`

appName : String value of the App/Service for which you are initialising
 
defaultLogLevel : The level beyond which the logs must be written to the backup log file. 

NOTE: Logs below the default level will still be sent to the DPA service, but will just not be written to the backup local log file.

This initialisation is to be done in the `main` method of your application or in the app.py file, right before the runserver statement.


##### **Logging**

After initialising as mentioned above, import the same package in all supporting files of the app you wish to log within, in the same manner:
 
    `from rr_python import rr_python as rr`
then at every point for logging, invoke the logging function as follows:

    `rr.rrAdapter(logLevel, message, logDict)`

logLevel: the level for the log message as in [info, debug, warning, critical or error]

message: the log message

logDict: a dictionary with extra details of the log. it MUST contain 2 fields:

*rrId* and *source*  
rrId: This is a unique id to track a request/ process end to end.
      Map rrId from the request body, if not present generate a random 4 uuid.
source: This field can be used to populate either solution name or service name(ex: [cvcm, pvcm] or [NER, TDE, submission-service])