import logging
from logging.handlers import RotatingFileHandler
import configparser
import time
from datetime import datetime
import urllib3
import requests
import os
import json
import requests


REACT_APP_APIURL = os.getenv("REACT_APP_APIURL", "https://api.df-dev.net")


def setupLogHandler(appName, defaultLogLevel):

    global fileHandler
    global logger
    global app 
    app = appName
    logger = logging.getLogger(appName)
    logger.setLevel(defaultLogLevel)    

    fileHandler = RotatingFileHandler(appName+"."+datetime.now().strftime("%d-%m-%y")+".log", maxBytes=10485760, backupCount=5, mode='w')  
    if (logger.hasHandlers()):
        logger.handlers.clear()
    logger.addHandler(fileHandler)

def rrAdapter(logLevel, message, logDict={}):
    logLevels = {
            "info":logger.info,
            "debug":logger.debug,
            "warning":logger.warning,
            "error":logger.error,
            "critical":logger.critical
            } 
    logTime = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+"Z" 
    dict_data =  str(logDict)[1:len(str(logDict))-1].replace("'", "\"")
    # log_message ="{" + f""""timestamp": "{logTime}","appName": "{app}", "level": "{logLevel.upper()}", "message": "{message}", """ + dict_data + "}"
    log_message_dict = logDict
    log_message_dict["timestamp"]=logTime
    log_message_dict["appName"]=app
    log_message_dict["level"]=logLevel.upper()
    log_message_dict["message"]=message
    
    path = "/dpaService/RR/init"
    req_path = REACT_APP_APIURL + path
    print(log_message_dict)


    req = requests.request("POST", req_path, json= log_message_dict)
    print(req.content)
    formatter = logging.Formatter("{\"timestamp\": \""+ logTime +"\",\"appName\": \""+app+"\", \"level\": \"%(levelname)s\", \"message\": \"%(message)s\", " + dict_data + "}")
    fileHandler.setFormatter(formatter)
    logLevels[logLevel.lower()](message)
    return
